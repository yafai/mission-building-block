# Concept proposal Process


## Forming new Teams

We would like Developers that are part of our project to split themselves into Teams. We recommend that you have atleast one CAD designer and one arduino programmer in each team. The idea behind this project is tackle a big issue that we may face soon in Oman (Lack of respiratory ventilation within our healthcare institutions due to low supply). 

Instructions on forming a Team


*  Once a team is formed please create a new directory (Folder) with your team's name
*  Download the [Team-signup.md](Team-signup.md) file, rename the file with your teams name and upload it to your directory 
*  Fill out the form the the necessary details of your team and begin working!

Please upload all of your relavant files for your designs in your directory to allow other memebers to comment and support you

This is not a competition between the teams and the goal is clear **We need to come up with something to save lives**

We expect team members to join us in the Mission Building Block [Discord](https://discord.gg/T4h9SJu) Server

We also encourage Team members to create their own servers in [Discord](https://discordapp.com/) 
