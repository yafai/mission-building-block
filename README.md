# Mission Building Block


Let's work together to come up with something great that saves lives

As we find ourselves against a faceless enemy threating everything, we hold dear our country and trajectory of our humanity; a sense of great responsibility motivates us, though distant from each other, to revive a modicum of hope in our world. COVID-19 has decided for us. Make no mistake about it, this is a mission laid on the lap of Omani innovators, manufacturers, young minds and patriots to solve for the unknown. Global healthcare systems have been overwhelmed, workers have been strung out of energy and the masses are imploring their governments as fear roams in the air. Now we come. For Oman.

Aim: Mass produce innovative, cheap and agile ventilation automation systems to combat the COVID-19 outbreak in Oman.
Due Date : April 4th (We encourage people to submit ideas even earlier, as we do need to act quickly)

Building Block: Inspired by HM Sultan Haitham Bin Tarq’s speech on the 23rd of Feb 2020: “Realizing the importance of … projects that are based on innovation, artificial intelligence and advanced technologies, and training and empowering young people to take advantage of the opportunities offered by this vital sector to be a basic building block in the national economy system, our government will work To keep track of progress in these aspects

# Low Resource Bag Valve Mask (BVM) Ventilator


*  This project was jumpstarted by the COVID-19 global pandemic as a result of the Omani communal responsibility to combat COVID19 by the SablaX team at OQ, Takatuf Scholars Alumni Association, Omani MOHE Scholars and Omani innovators. For this reason  “Mission Building Block” a GitLab project, was created to design an open-source product.
*  More specifically, this project was created in a discussion surrounding low cost Bag Valve Mask (BVM or AmbuBag)-based emergency respirators wherein prior solutions had been developed.
Examples: An early device discussed came from an MIT research group comprising of the following persons: Abdul Mohsen Al Husseini, Heon Ju Lee, Justin Negrete, Stephen Powelson, Amelia Servi, Alexander Slocum and Jussi Saukkonen. A second device arose from a Rice University Mechanical Engineering student group comprising of the following persons: Madison Nasteff, Carolina De Santiago, Aravind Sundaramraj, Natalie Dickman, Tim Nonet and Karen Vasquez Ruiz.
This project seeks to come up, combine and improve efforts around creating a low cost BVM of these two projects into a more simple and reliable device that consists mostly of easy to source or 3D printed parts.
*  	Benefits: Can be mass-produced, touch points use certified components, small and simple mechanical requirements, previous research and testing in this area, adaptable to both invasive tubing and masks.
WARNING/DISCLAIMER: Whenever possible, please seek professional medical care with proper equipment setup by trained individuals. Do not use random information you found on the internet. We are not medical professionals, just volunteers on the internet.

# How to Help


1.  Fill out our Mission Building Block Expression of Interest Form [here](https://forms.gle/ZyrxfUtJCtLrnU1N9) if you haven't already. This step is mandatory and it’s how we will find you to match your skills to areas you can help.
2.  We will reach out when your noted skills are needed, until then please check out the following: -- Stay up to date with the project here, and our social channels. -- Join our WhatsApp group the conversation.
3.  Get familiar with git, to learn how to best contribute to this repository. There are many resources available online, you can start with YouTube: Introduction to GitLab Workflow or learn how to submit a merge request in GitLab's docs.

## Aim
Come up with a design using existing designs from MIT and Rice University Mechanical Engineering student group designs as a reference. The design should automate the process of manual ventilation via a Bag Valve Mask and Oxygen feed.

## Objectives

*  Design something simple yet effective
*  The design should be capable on running on a low amp motor
*  The device should have a interface (LCD Screen and buttons)
*  The device should contain feedback sensors for PEEP, Voltage, Pressure

## Design Requirements

This was taken from the OpenLung BVM and it summarises everything needed for the design. Please have a look [here](https://gitlab.com/habib1295/OpenLung/-/blob/master/requirements/design-requirements.md)

## Delivery date

**Workspace**

30/03/2020

**Design** 

04/04/2020

**Production**

## Needed Skills

*  Coding
*  Arduinos programing
*  CAD design
*  product design
*  prototyping
*  Project management

## Sucess Criteria

*  Create a design that's easy to produce
*  Parts for the design should be locally avaiable and easy to source 
*  The design can be mass produced
*  Tests for the prototype are sucessful
*  The design adapts to whats available in terms of invasive tubing and masks

## Assumptions, Risks, Obstacles

*  Assume improving on MIT research group and Rice University Mechanical Engineering student group AmbuBag based emergency respirator based on known issues with those designs.
*  Assume communication would take place in Discord. 
*  Assume the GitLab page is the the only source for documents and information

## Targeted participants

*  SablaX team at OQ
*  Takatuf Scholars Alumni Network
*  Subul
*  Omani MOHE scholars
*  Omani innovators


## Workspace 

*  Gitlab
*  Discord
*  Whatsapp

## Design Software 

*  [Autocad](https://www.autodesk.com/education/free-software/autocad)
*  [Solidworks](https://www.solidworks.com/) 
*  [Fusion360](https://www.autodesk.com/products/fusion-360/students-teachers-educators)
*  [CATIA](https://www.3ds.com/products-services/catia/?wockw=card_content_cta_1_url%3A%22https%3A%2F%2Fblogs.3ds.com%2Fcatia%2F%22)
*  etc.

## Coding Software

*  [Arduino](https://www.arduino.cc/en/Main/Software)
*  [Python](https://www.python.org/downloads/)
*  [Matlab](https://www.mathworks.com/products/get-matlab.html)
*  [Visual Studio](https://visualstudio.microsoft.com/)
*  etc..


**Note**

Some of the information in our repository was referenced from the OpenLung BVM Ventilator. 


## Flow Chart
This **flow chart** was created by our peers at OpenLung BVM Ventilator please have a look at the overview of the flow design of the system [here](https://whimsical.com/4mai9JtqTHAGu6i6qz8Hyy)

This **flow chart** created by our peers at OpenLung BVM Ventilator shows the all of the design aspects that we need to consider for the design               [here](requirements_overview-flowchart.pdf)

## Some Useful links on Design and prototyping low cost ventilators



**RT Clinic** : Manual ventilation with a bag valve mask

https://www.youtube.com/watch?v=_eV7_HuMDL4

**Design and Prototyping of a Low-cost Portable Mechanical Ventilator**

https://e-vent.mit.edu/wp-content/uploads/2020/03/DMD-2010-MIT-E-Vent.pdf

**Open Source Ventilator concept designs**

https://gitlab.com/open-source-ventilator/OpenLung/-/tree/master/concepts

**University of Oxford Ventilator project**

https://oxvent.org/

